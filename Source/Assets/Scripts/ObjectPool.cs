﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Puts GameObjects in a pool so that they can be reused later if needed (instead of instantiating/destroying new GameObjects)
public class ObjectPool : MonoBehaviour
{
	// static refernece to the class...making it a singleton essentially
	public static ObjectPool current;
	public int initialPoolSize = 50;

	private List<GameObject> pool;

	// initialize the pool
	void Awake()
	{
		current = this;
		pool = new List<GameObject>(initialPoolSize);
	}

	// (used instead of Instantiate)
	// returns an instance of the clone GameObject from the object pool if possible, or a new instance otherwise.
	public GameObject GetObject(GameObject clone, Vector3 position, Quaternion rotation, bool activate = false)
	{
		GameObject obj = null;

		// loop through the pool and see whether we can use any instance from the pool
		foreach (GameObject o in pool)
		{
			// check the name of the pooled object against the one that we need.
			// the name field of cloned GameObjects automatically have '(Clone)' string appended to their name
			// then see whether it can be reused (if it's inactive)
			if (o.name.Replace("(Clone)", "") == clone.name && !o.activeInHierarchy)
			{
				obj = o;
				break;
			}
		}

		// if the whole pool is used up, instantiate a new clone
		if (obj == null)
		{
			obj = Instantiate(clone, position, rotation);
			pool.Add(obj);
		}

		// apply the transform passed in the args
		ApplyTransform(obj, position, rotation);

		// activate the object if need be
		obj.SetActive(activate);

		return obj;
	}

	// returns a pooled GameObject if possible, null otherwise
	public GameObject GetPooledObject(GameObject clone)
	{
		foreach (GameObject obj in pool)
		{
			if (obj.name == clone.name && !obj.activeInHierarchy)
				return obj;
		}

		return null;
	}

	// same as GetPooledObject() but allows us to set the position, rotation and active state
	public GameObject GetPooledObjectAtPosition(GameObject clone, Vector3 position, Quaternion rotation, bool activate = false)
	{
		GameObject obj = GetPooledObject(clone);
		if (obj != null)
		{
			ApplyTransform(obj, position, rotation);
			obj.SetActive(activate);
		}

		return null;
	}

	// deactivates a GameObject (used instead of Destroy)
	public void Deactivate(GameObject obj)
	{
		obj.SetActive(false);
	}

	// deactivates a GameObject after a delay (seconds)
	public IEnumerator DeactivateDelay(GameObject obj, float delay)
	{
		yield return new WaitForSeconds(delay);
		Deactivate(obj);
	}

	// helper method that applies some transform to a given GameObject
	private void ApplyTransform(GameObject obj, Vector3 position, Quaternion rotation)
	{
		obj.transform.position = position;
		obj.transform.rotation = rotation;
	}
}
