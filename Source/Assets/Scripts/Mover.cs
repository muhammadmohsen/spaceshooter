﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// used to move both gun shots and astroids
public class Mover : MonoBehaviour
{
	public float speed;

	// Executes in the first frame where the GameObject is instantiated
	void Start()
	{
		Rigidbody rigidbody = GetComponent<Rigidbody>();

		// give the bolt a velocity in the forward direction (z-axis)
		rigidbody.velocity = transform.forward * speed;
	}
}
