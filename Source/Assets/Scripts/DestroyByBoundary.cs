﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour
{
	// deactivate anything that leaves the game area (bolts/astroids/enemy ships)
	void OnTriggerExit(Collider other)
	{
		ObjectPool.current.Deactivate(other.gameObject);
	}
}
