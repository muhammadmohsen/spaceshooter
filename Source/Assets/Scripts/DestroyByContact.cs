﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
	public GameObject explosion = null; // generic explosion (used on astroids)
	public GameObject playerExplosion;

	public int scoreValue; // destruction score (for example: astroids give 10 points)

	private GameController gameController; // reference to the GameController class (script)

	void Start()
	{
		GameObject controllerGameObject = GameObject.FindGameObjectWithTag("GameController"); // get the GameController GameObject
		if (controllerGameObject != null)
			gameController = controllerGameObject.GetComponent<GameController>(); // get the GameController class (script component)

		if (gameController == null)
			Debug.Log("Cannot find 'gameController' script.");
	}

	// called when a collision occurs on the trigger collider component
	void OnTriggerEnter(Collider other)
	{
		// ignore collisions with the boundary and other enemies
		if (other.CompareTag("Boundary") || other.CompareTag("Enemy"))
			return;

		// instantiate the explosion game object (if it exists)
		if (explosion != null)
		{
			ObjectPool.current.GetObject(explosion, transform.position, transform.rotation, true);
		}

		if (other.CompareTag("Player"))
		{
			// explode the player
			ObjectPool.current.GetObject(playerExplosion, other.transform.position, other.transform.rotation, true);
			// finish the game
			gameController.GameOver();
		}

		gameController.AddScore(scoreValue);

		// destroy both the astroid and the bolt
		// Destroy() doesn't immediately destroy the GameObejct, it marks the objects to be destroyed at the end of the frame
		ObjectPool.current.Deactivate(other.gameObject);
		ObjectPool.current.Deactivate(gameObject);
	}
}
