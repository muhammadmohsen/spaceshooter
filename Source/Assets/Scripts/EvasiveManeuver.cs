﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// responsible for enemy ships random maneuvers
public class EvasiveManeuver : MonoBehaviour
{
	// the game boundary (class definition in PlayerController.cs)
	public Boundary boundary;

	// maneuver variables
	public float smoothing;
	public float roll; // ship roll
	public float dodge; // maneuver amount along the x-axis

	// the Vector2 class is used as a two-value (min, max) container.
	// the two values are used as the applicable random range.
	public Vector2 startWait; // time to wait until the maneuver is started
	public Vector2 maneuverWait; // time to wait between maneuvers
	public Vector2 maneuverTime; // the time it takes to actually maneuver the ship

	// the target maneuver velocity (x component of the velocity vector)
	private float targetManeuver;

	// the new keyword indicates that base member hiding is intentional
	new private Rigidbody rigidbody = null;

	// code is moved to OnEnable to have the StartCoroutine() called every time the object is enabled
	void OnEnable()
	{
		if (rigidbody == null)
			rigidbody = GetComponent<Rigidbody>();

		StartCoroutine(Evade());
	}

	// because we're using an object pool, we have to stop all active Coroutines/Invokes in OnDisable()
	// so we can have a consistent inital state when reusing an object instance
	void OnDisable()
	{
		StopCoroutine(Evade());
	}

	void FixedUpdate()
	{
		// calculate the instantaneous maneuver velocity (x component of the velocity vector)
		float newManeuver = Mathf.MoveTowards(rigidbody.velocity.x, targetManeuver, Time.deltaTime * smoothing);
		rigidbody.velocity = new Vector3(newManeuver, 0.0f, rigidbody.velocity.z);

		// clamp the ship's x position within the playing area
		rigidbody.position = new Vector3
		(
			Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp(rigidbody.position.z, boundary.zMin, boundary.zMax)
		);

		// roll the ship as it's moving left or right depending on its velocity!
		rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, rigidbody.velocity.x * -roll);
	}

	IEnumerator Evade()
	{
		// wait for a random amount before starting to maneuver
		yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

		while (true)
		{
			// calculate a target maneuver (a random value for the x component of the velocity) then wait for a random amount of time (see FixedUpdate for the actual maneuvering)
			targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);
			yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));

			// then, stop maneuvering and wait for the inter-maneuver time
			targetManeuver = 0;
			yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
		}
	}
}
