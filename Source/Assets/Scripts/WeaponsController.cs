﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsController : MonoBehaviour
{
	public GameObject shot;
	public Transform shotSpawnTransform;

	public float fireRate;
	public float delay;

	private AudioSource audioSource = null;

	// OnEnable() gets called every time the GameObject is enabled.
	// Important note: OnEnable() is called BEFORE Start()
	void OnEnable()
	{
		// because OnEnable is called more than once, we'll test to see if we already got the audioSource component
		// also, because OnEnable is called before Start we can't call GetComponent in Start as the audioSource.Play() call will fail when we first call Fire()
		if (audioSource == null)
			audioSource = GetComponent<AudioSource>();

		InvokeRepeating("Fire", delay, fireRate);
	}

	void OnDisable()
	{
		CancelInvoke();
	}

	void Fire()
	{
		ObjectPool.current.GetObject(shot, shotSpawnTransform.position, shotSpawnTransform.rotation, true);
		audioSource.Play();
	}
}
