﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
	// movement speed and ship roll multipliers
	public float speed;
	public float roll;

	// game area boundries
	public Boundary boundary;

	// the shot (Bolt prefab)
	public GameObject shot;
	// the shot spawn point's Transform component
	public Transform shotSpawnTransformComponent;

	// minimum time between shots (fire rate)
	public float fireDelta = 0.25f;
	// the nearest time in which firing is available
	private float nextFire = 0.0f;

	// Update is called once per frame
	void Update()
	{
		// if Fire1 (default: Ctrl) is pressed and at least deltaTime has elapsed since we last fired a shot, shoot another
		if (Input.GetButton("Fire1") && Time.time > nextFire)
		{
			// calculate nextFire time
			nextFire = Time.time + fireDelta;

			// instantiate creates the specified object at the specified position and rotation
			ObjectPool.current.GetObject(shot, shotSpawnTransformComponent.position, shotSpawnTransformComponent.rotation, true);
			// play the attached audio source
			GetComponent<AudioSource>().Play();
		}
	}

	// executed once every physics step
	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");

		Rigidbody rigidbody = GetComponent<Rigidbody>();
		rigidbody.velocity = new Vector3(moveHorizontal, 0.0f, moveVertical) * speed;

		// clamp the player movement into the play area
		rigidbody.position = new Vector3
			(
				Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
				0.0f,
				Mathf.Clamp(rigidbody.position.z, boundary.zMin, boundary.zMax)
			);

		// roll the player as it's moving left or right depending on its velocity!
		rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, rigidbody.velocity.x * -roll);
	}

	// the serializable annotation is necessary to make objects of it visible in the Unity editor
	[System.Serializable]
	public class Boundray
	{
		public float xMin, xMax, zMin, zMax;
	}
}
