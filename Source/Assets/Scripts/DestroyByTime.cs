﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// destroys the attached GameObject after a time delay (used for explosion animations)
public class DestroyByTime : MonoBehaviour
{
	public float lifeTime;

	void OnEnable()
	{
		StartCoroutine(ObjectPool.current.DeactivateDelay(gameObject, lifeTime));
	}

	void OnDisable()
	{
		StopCoroutine("ObjectPool.current.DeactivateDelay");
	}
}
