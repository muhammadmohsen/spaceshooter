﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// controls the game!
// spawns hazards (astroids) displays/updates the GUI and restarts the game
public class GameController : MonoBehaviour
{
	public GameObject[] hazards;
	public Vector3 spawnValues;

	// astroid wave configurations
	public int hazardCount;
	public int startWait;
	public int spawnWait;
	public int waveWait;

	// GUI
	public Text scoreText;
	public Text gameOverText;
	public Text restartText;

	// score and flags that control the GUI
	private int score;
	private bool gameOver;
	private bool restart;

	// initialization
	void Start()
	{
		gameOver = false;
		restart = false;

		restartText.text = "";
		gameOverText.text = "";

		score = 0;
		UpdateScore();

		// call an "async" method
		StartCoroutine(SpawnWaves());
	}

	void Update()
	{
		if (restart)
		{
			if (Input.GetKeyDown(KeyCode.R))
				SceneManager.LoadScene("Main", LoadSceneMode.Single);
		}
	}

	// spawns waves of astroid hazards
	// IEnumerator MUST be the return type for Unity's Coroutines (a kind of async method)
	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds(startWait); // this is how long we will wait at the start of the wave

		while (true)
		{
			for (int i = 0; i < hazardCount; i++)
			{
				// random X spawn position
				Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				// zero rotation
				Quaternion spawnRotation = Quaternion.identity;

				// spawn an astroid (get one from the object pool if possible)
				GameObject hazard = hazards[Random.Range(0, hazards.Length)];
				ObjectPool.current.GetObject(hazard, spawnPosition, spawnRotation, activate: true);

				yield return new WaitForSeconds(spawnWait);
			}

			yield return new WaitForSeconds(waveWait);

			// wait for the astroid wave to finish before giving the player the ability to restart
			if (gameOver)
			{
				restartText.text = "Press 'R' to Restart";
				restart = true;

				break;
			}
		}
	}

	public void AddScore(int addedScore)
	{
		score += addedScore;
		UpdateScore();
	}
	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
	}

	public void GameOver()
	{
		gameOverText.text = "Game Over";
		gameOver = true;
	}
}
