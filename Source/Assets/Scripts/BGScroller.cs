﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// scrolls the game's nebula background
public class BGScroller : MonoBehaviour
{
	public float scrollSpeed; // the scroll speed
	public float tileSizeZ; // the size of the background tile (quad)

	private Vector3 startPosition; // the starting position of the background quad

	void Start ()
	{
		startPosition = transform.position;
	}

	void Update ()
	{
		// calculate the new tile position and set it
		// the Mathf.Repeat is essentially a MOD. The first argument is looped around the second argument
		// so that it is never larger than the first argument and never smaller than 0
		float newPositionZ = Mathf.Repeat(Time.time * scrollSpeed * -1, tileSizeZ);
		transform.position = startPosition + Vector3.forward * newPositionZ;
	}
}
